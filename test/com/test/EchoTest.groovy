package com.test

import static org.junit.Assert.*;

import org.junit.Test;

class EchoTest {

    @Test
    void shouldReturnString() {
        def echo = new Echo()
        assertEquals("blah", echo.echo("blah"))
    }
}
